# Documentaion for galli-ci nodes 

Resources managed by RCC that is used by the WEST software development project CI pipeline. 

## Resource Details
Total Number of nodes: 2   	<br />
Base components of each node: 	<br />
CPU: Intel Xeon Proc E5-2680 (Broadwell)	<br />
2.4GHz, 28 cores, 35 MB Cache 2400 MHz	<br />
Memory: 64GB	<br />
Network card: FDR (54 Gbps)	<br />
Local storage disk type: Solid State Drive (SSD)	<br />
Solid State Drive (SSD) specs: s3610  Enterprise SATA; 6Gbps	<br />
Size of individual SSD: 1.2 TB 	<br />
Hardware MOA Expiration is July, 2021. 	<br />

CI Build Cluster Details: 	<br />
Operating System: Scientific Linux 7	<br />
File system: drives put on one node (galli-ci01), fs exported to other node via NFS	<br />
Total Size of file system: 2.4 TB	<br />
RAID configuration: RAID level 0 	<br />
Hostnames:  galli-ci01 galli-ci02 	<br />
IP addresses: 172.25.119.51    172.25.119.52	<br />

#### Notes: 	<br />
* No slurm scheduler on these nodes; requires setting up host.list file for mpi jobs
* No gpfs client license as is not using gpfs natively; software directory is exported via NFS.
* Accessible module system is midway2 module system since nodes are SL7.
* The home directories are local to nodes; home on galli-ci nodes is not the same home as on midway2. This was set up specifically in this way so that in the event there is an issue with the gpfs file system of midway, one can still access galli-ci nodes remotely since it does not require accessing anything from the user's gpfs home directory. 
* Hostnames presently only resolvable within the RCC network, use ip addresses to connect from outside the midway network. 
* software builds rely on compilers and libraries in the software file system (GPFS) of midway2 that is exported via NFS. The containers that build WEST software do not rely on any of the software in /software; each container build is self-contained and originates from greatfire.uchicago.edu where the WEST project has a self-hosted gitlab instance. 

## Gitlab-runners: 

* The docker-compose.yaml file that is used by the gitlab-runner container resides in the `/home/gitlab-runner directory`. 
* The config file for the gitlab-runner is located in `/home/gitlab-runner/config`   
* There is a gitlab-runner account, which should be used to start the gitlab-runner container. 
* Inspect the [gitlab-runner docker-compose.yml](./docker-compose.yml) file: 

Check that gitlab-runner container is up and running: 

```
docker ps -f "name=gitlab"
```
 
This should return the docker process if gitlab is running. If nothing is returned, the container process is not running. One can restart it by switching to the `gitlab-runner` account and then running docker-compose: 

``` 
su - gitlab-runner
docker-compose up -d
```
 

## Troubleshooting common issues: 

 
The Docker local images on `galli-ci01` are stored in `/var/lib/docker` which is on the rootfs. The rootfs only has a total size of 32GB, and typical images are MB to GB in size. There can sometimes be an issue that there is baggage left behind  and you may need to clean up any image layers that are not in use. This can be done w/ the following command: 

```
docker rmi $(docker images --filter "dangling=true" -q --no-trunc)
```
 
You can check the set of images stored with the following: 

``` 
docker images 
```
 
The docker info command can also give you info on number of containers, what’s running, what’s stopped, as well as additional info about the setup and system. 

``` 
docker info 
```

### Rebooting galli-ci0[1-2] nodes:
 
If the nodes require rebooting, be aware that they are net-booted. The [gallici postscript](./gallici.postscript) file contains most of the configuration settings and services that are applied to the net-booted images. Presently one needs to additionally do the following manually after reboot in order to resume using the gitlab-runners: 
 
```
useradd gitlab-runner
usermod -a -G docker gitlab-runner
su - gitlab-runner
docker-compose up -d
```
 
Provided there is no issue w/ the NVRAM, the nodes should start up services upon reboot with the above placed in postscript. 
 
